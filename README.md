Project Name
Nomad Mobile

Project Tagline/Description (140 Characters Max. Will be used on table card for judging)
Truly decentralized, no middle-man social networking application for mobile

Team Members. First and Last Names
Mark Harding
Emi Balbuena
Brian Hatchet
Olivia Madrid
Status.im ID for Each Team Member (we will use this to contact you and your team)
https://get.status.im/user/0x0466f87c7f2d2f818f9007525aeeebc95c45a2b409ac3933cf4d8206cddc7723be4678f85a06d00bc1557d30011f057b1b478742ba760bd372babd60ae7d98fd23 / mark@minds.com

Detailed Project Description (no more than 3-4 sentences)
Nomad is a fully decentralized, open source social network that requires no middle-man. User own their own data and the blockchain is used only for when consistency is required (eg. knowing someones username). The Nomad Mobile application used a hybrid of the Ethereum Network and the DAT protocol.

Describe your tech stack (e.g., protocols, languages, API’s, etc.)
React Native
Nodejs
DAT
Web3
Ethereum
Track for which you’re submitting (Open or Impact)
Impact

A link to all your source code on a public repo (i.e. Github)
https://gitlab.com/minds/nomad-mobile https://rinkeby.etherscan.io/address/0x0093b9e1fcd792b9876a2281af876de50595d24c -> usernames contract