var rnBridge = require('rn-bridge');

var Dat = require('dat-node');
var storage = require('dat-storage');
var hyperdrive = require('hyperdrive');
const { findFreePort } = require('./utils');

module.exports.init = async (path) => {

    //first create the secrets directory
    const secrets = hyperdrive(path + '/nomad-secrets');
    await secrets.mkdir('/.dat');

    //now create our empty nomad-repo
    const dir = path + '/nomad-repo';
    const archive = hyperdrive(dir);
    await archive.writeFile('/dat.json', '{ title: "Nomad" }');

    try {

        Dat(dir, { secretDir: path + '/nomad-secrets' }, async (err, dat) => {
            rnBridge.channel.send("returned dat");

            if (err)
                return rnBridge.channel.send('err: ' + err.message);

            dat.importFiles();

            // Get a free port
            const port = 3282;

            dat.joinNetwork();

            rnBridge.channel.send('My Dat link is: dat://' + dat.key.toString('hex'));
            rnBridge.channel.send('Port is ' + port);

            dat.network.on('connection', function () {

                rnBridge.channel.send("connected");

            });

            /*setInterval(() => {
                var stats = dat.trackStats();
                rnBridge.channel.send(JSON.stringify(stats.network));
                    rnBridge.channel.send(JSON.stringify(stats.peers));
            }, 1000);*/
        });
    } catch (err) {
        rnBridge.channel.send('exception: ' + err.message);
    }
}

