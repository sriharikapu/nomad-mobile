import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Button,
  Image
} from 'react-native';
import Colors from '../Colors';
import authService from '../services/auth.service';

export default class LoginScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      allowedAction: null,
      username: '',
    };
  }

  login = () => {
    this.props.navigation.replace('Home');
  }

  onChangeUsername(value) {
    this.setState({
      username: value,
    });

    if (this.$onChangeUsernameTimer) {
      clearTimeout(this.$onChangeUsernameTimer);
    }

    this.$onChangeUsernameTimer = setTimeout(() => this.lookupUsername());
  }

  async lookupUsername() {
    if (!this.state.username) {
      return;
    }

    try {
      const datUri = await authService.getUserDatUri(this.state.username);

      this.setState({
        allowedAction: datUri ? 'login' : 'register'
      });
    } catch (e) {
      console.log(e);

      this.setState({
        allowedAction: null
      });
    }
  }

  componentWillUnmount() {
    if (this.$onChangeUsernameTimer) {
      clearTimeout(this.$onChangeUsernameTimer);
    }
  }

  doAction() {
    const { username } = this.state;

    switch (this.state.allowedAction) {
      case 'login':
        this.props.navigation.push('LoginKey', { username });
        break;

      case 'register':
        this.props.navigation.push('Register', { username });
        break;
    }
  }

  render() {
    const { allowedAction } = this.state;

    return (
      <View style={styles.container}>
      <Image 
          source={require('../../assets/nomad.png')}
          style={{width: 120, height: 100}}
          resizeMethod="scale"
          />
        <TextInput
          value={this.state.username}
          onChangeText={value => this.onChangeUsername(value)}
          placeholder="Username"
          autoCapitalize="none"
          autoCorrect={false}
          style={[styles.loginInput]}
        />

        {Boolean(allowedAction) && <Button
          title={allowedAction === 'login' ? 'Login' : 'Register'}
          onPress={this.doAction.bind(this)}
          style={[styles.loginButton]}
        />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  loginInput: {
    marginTop: 32,
    paddingLeft: 16,
    width: 250,
    borderRadius: 24,
    borderWidth: 1,
    borderColor: '#e8e8e8',
  },
  loginButton:{
    marginTop:10,
    borderWidth: 1,
    padding: 16,
    fontSize: 15,
    borderRadius: 20,
    color: Colors.primary,
    backgroundColor: 'transparent',
    borderColor: '#e8e8e8'
  },
  // logo: {
  //   width: 212,
  //   height: 78
  // }
});