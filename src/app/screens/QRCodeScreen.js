import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import QRCodeReader from '../components/QRCodeReader';

type Props = {};

export default class QRCodeScreen extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <QRCodeReader/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});