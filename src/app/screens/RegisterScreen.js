import React, { Component } from 'react';
import { Button, View, StyleSheet, ActivityIndicator } from 'react-native';
import web3Service from '../services/web3.service';
import authService from '../services/auth.service';

export default class RegisterScreen extends Component {
  static navigationOptions = {
    title: 'Register',
  };

  constructor(props) {
    super(props);

    this.state = {
      creating: false
    };
  }

  async createAddress() {
    this.setState({
      creating: true
    });

    try {
      const { privateKey } = web3Service.createWallet();
      const username = this.props.navigation.getParam('username');
      const profile = await authService.register(username, privateKey);

      if (profile) {
        this.props.navigation.replace('Home');
      }
    } catch (e) {
      console.warn(e);
    }
  }

  importAddress() {
    this.props.navigation.push('RegisterKey', { username: this.props.navigation.getParam('username')});
  }

  render() {
    return (
      <View style={styles.container}>
        <Button
          title="Create Address"
          onPress={this.createAddress.bind(this)}
        />

        <Button
          title="Use Existing Address"
          onPress={this.importAddress.bind(this)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});