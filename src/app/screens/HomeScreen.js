import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Nomad',
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>WELCOME TO NOMAD. You are now connected to the DAT protocol</Text>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});