import React, { Component } from 'react';
import {
  View,
  TextInput,
  Button,
  StyleSheet,
} from 'react-native';
import QRCodeReader from '../components/QRCodeReader';

import authService from '../services/auth.service';
import web3Service from '../services/web3.service';

export default class RegisterKeyScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      inProgress: false,
      privateKey: '',
    };
  }

  async setPrivateKey(privateKey) {
    this.setState({ privateKey });

    const username = this.props.navigation.getParam('username');

    try {
      const address = web3Service.getAddressFromPK(privateKey);

      if (address) {
        this.setState({
          inProgress: true,
        });

        const profile = await authService.register(username, privateKey);

        if (profile) {
          this.props.navigation.replace('Home');
        }
        else {
          this.setState({
            inProgress: false,
          });
        }
      }
    } catch (e) {
      this.setState({
        inProgress: false,
      });
      console.warn(e);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={[styles.pkInput]}
          value={this.state.privateKey}
          onChangeText={this.setPrivateKey.bind(this)}
          placeholder="Existing Private Key"
        />

        <View style={styles.qrCodeReader}>
          {!this.state.inProgress && <QRCodeReader onBarcode={this.setPrivateKey.bind(this)} />}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  pkInput: {
    height: 50,
  },
  qrCodeReader: {
    width: '100%',
  }
});