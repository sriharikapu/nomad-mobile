import { createStackNavigator } from "react-navigation";

import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import LoginKeyScreen from "../screens/LoginKeyScreen";
import RegisterScreen from "../screens/RegisterScreen";
import RegisterKeyScreen from "../screens/RegisterKeyScreen";

const AppNavigator = createStackNavigator({
    Login: {
      screen: LoginScreen
    },
    LoginKey: {
      screen: LoginKeyScreen
    },
    Register: {
      screen: RegisterScreen
    },
    RegisterKey: {
      screen: RegisterKeyScreen
    },
    Home: {
      screen: HomeScreen
    }
  },
  {
    initialRouteName: "Login"
  });

export default AppNavigator;