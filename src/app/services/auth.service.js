import { UsernamesContract } from "./blockchain/usernames/contract";
import web3Service from "./web3.service";
import { Profile } from "../entities/Profile";

const web3 = web3Service.web3;

class AuthService {
  async getUserDatUri(username) {
    try {
      const result = await UsernamesContract.methods.getUsernameDatUri(web3.utils.toHex(username)).call();

      if (result) {
        return web3.utils.toUtf8(result);
      }
    } catch (e) { 
      console.warn(e);
    }

    return null;
  }

  async register(username, privateKey) {
    const datUri = 'nomad.minds.com';

    try {
      const address = web3Service.getAddressFromPK(privateKey);

      const register = UsernamesContract.methods.register(
        web3.utils.toHex(username),
        address,
        web3.utils.toHex(datUri)
      );

      const tx = await web3Service.sendSignedContractMethod({ address, privateKey }, register, 'Register to Nomad');

      if (!tx) {
        return false;
      }

      // TODO: Write JSON profile to dat
      // this.datArchive.writeFile('/data/profile.json', JSON.stringify(profile));

      // TODO: Fork!

      return new Profile(username, address, datUri);
    } catch (e) {
      console.warn(e);
    }

    return false;
  }

  async login(username, privateKey) {
    try {
      const address = web3Service.getAddressFromPK(privateKey);
      const owner = await UsernamesContract.methods.getUsernameOwner(web3.utils.toHex(username)).call();

      if (owner.toLowerCase() !== address.toLowerCase()) {
        return false;
      }

      const datUri = await UsernamesContract.methods.getUsernameDatUri(web3.utils.toHex(username)).call();

      return new Profile(username, address, datUri);
    } catch (e) {
      console.error(e);
    }

    return false;
  }
}

export default new AuthService;