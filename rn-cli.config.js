const blacklist = require('metro-config/src/defaults/blacklist');
const extraNodeModules = require('node-libs-browser');

module.exports = {
  extraNodeModules,
  resolver:{
    blacklistRE: blacklist([
      /nodejs-assets\/.*/,
      /android\/.*/,
      /ios\/.*/
    ])
  },
};

