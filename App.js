/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

// React Native + Node modules
import './shim'
import './global';
//

import React, { Component } from 'react';
import {
  Observer,
  Provider,
} from 'mobx-react/native'
import { createAppContainer } from 'react-navigation';
import nodejs from 'nodejs-mobile-react-native';
import RNFS from 'react-native-fs';
import MainNavigation from './src/app/navigation/MainNavigation';
import stores from './src/app/Stores';
import Modal from './src/app/components/payments/Modal';
const NavigationStack = createAppContainer(MainNavigation)

export default class App extends Component {
  componentWillMount() {
    nodejs.start("main.js");
    nodejs.channel.addListener(
      "message",
      (msg) => {
        //alert("From node: " + msg);
        console.log('message ' + msg);
      },
      this
    );
    nodejs.channel.send(JSON.stringify({
      method: 'init',
      path: RNFS.DocumentDirectoryPath,
    }));
  }

  render() {
    return (
      <React.Fragment>
        <Provider key="app" {...stores}>
          <NavigationStack />
        </Provider>

        <Modal />
      </React.Fragment>
    )
  }
}
